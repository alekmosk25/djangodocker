FROM python:3-alpine

WORKDIR /code
COPY . /code/
RUN pip install -r requirements.txt

RUN python3 manage.py migrate
RUN echo "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@example.com', 'pass')" | python3 manage.py shell

EXPOSE 8000
CMD python3 manage.py runserver 0.0.0.0:8000
